import '@babel/polyfill'

import express from 'express'
import cors from 'cors'
import path from 'path'

import { PORT } from './config/config.json'
import upload from './routes/upload-file'
import { ATTACHMENT } from './config/config.json'

const app = express()

app.use(cors())
app.post('/upload', upload.array('files', 100), (req, res) => {
  try {
    res.send(req.files)
    console.log('success upload ' + path.join(__dirname, ATTACHMENT.UPLOAD_PATH) + '/' + req.files[0].filename)
  } catch (error) {
    console.error(error)
    res.send(error.toString()).status(500)
  }
})
app.use(express.json())
app.get('/download', function (req, res) {
  const file = `${__dirname}` + ATTACHMENT.UPLOAD_PATH + '/' + req.body.filename
  console.log('success download ' + file)
  res.download(file)
})
app.listen({ port: PORT }, () => console.log(`Express Server start at port ${PORT}`))