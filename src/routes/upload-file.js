import multer from 'multer'
import moment from 'moment'
import path from 'path'
import { ATTACHMENT } from '../config/config.json'

const uniqNameGenerator = () =>
  Math.random().toString(36).substr(2, 9) + '-' + moment().format('X')

const storage = multer.diskStorage({
  destination: (_req, _file, cb) => {
    cb(null, path.join(__dirname, '../' + ATTACHMENT.UPLOAD_PATH) + '/')
  },
  filename: (_req, file, cb) => {
    cb(null, uniqNameGenerator() + path.extname(file.originalname))
  },
})

const fileFilter = (_req, file, cb) => {
  if (
    file.mimetype == 'image/jpeg' ||
    file.mimetype == 'image/vnd.microsoft.icon' ||
    file.mimetype == 'image/gif' ||
    file.mimetype == 'image/x-icon' ||
    file.mimetype == 'image/png' ||
    file.mimetype == 'image/svg+xml' ||
    file.mimetype == 'video/mp4' ||
    file.mimetype == 'video/x-matroska' ||
    file.mimetype == 'application/x-mpegURL' ||
    file.mimetype == 'video/MP2T' ||
    file.mimetype == 'video/3gpp' ||
    file.mimetype == 'video/quicktime' ||
    file.mimetype == 'video/x-msvideo' ||
    file.mimetype == 'video/ogg' ||
    file.mimetype == 'video/webm' ||
    file.mimetype == 'video/x-ms-wmv' ||
    file.mimetype == 'application/pdf' ||
    file.mimetype == 'image/bmp'
  ) {
    cb(null, true)
  } else {
    cb(null, false)
  }
}

const upload = multer({ storage: storage, fileFilter: fileFilter })
export default upload
